import 'dart:io';

import 'package:dart_oop/dart_oop.dart' as dart_oop;

abstract class Hero {
  int attack = 0;
  int hp = 0;

  Hero(int attack, int hp) {
    this.attack = attack;
    this.hp = hp;
  }

  get getAttack => this.attack;

  set setAttack(attack) => this.attack = attack;

  get getHp => this.hp;

  set setHp(hp) => this.hp = hp;

  void normalAttack();
  void skill1();
  void skill2();
  void skill3();

  void menuList() {
    print("1.Normal Attack");
    print("2.Skill 1");
    print("3.Skill 2");
    print("4.Skill 3");
    print("5.Passive Skill");
  }
}

class PassiveWarrior {
  void regen() {}
}

class PassiveMage {
  void perfectCast() {}
}

abstract class Warrior extends Hero implements PassiveWarrior {
  Warrior(int attack, int hp) : super(attack, hp);
}

abstract class Mage extends Hero implements PassiveMage {
  Mage(int attack, int hp) : super(attack, hp);
}

class Pareena extends Mage {
  String? name;
  double totalDamage = 0;
  Pareena(String name) : super(50, 300) {
    this.name = name;
  }

  @override
  void perfectCast() {
    print("Damage skill เพิ่มขึ้น 10 ในการโจมตีครั้งถัดไป");
  }

  @override
  double skill1() {
    totalDamage = (getAttack * (20 / 2)) / 10;
    print("ใช้ทักษะในการพูดจา สร้างความเสียหายเวทแก่ศัตรู 20");
    return totalDamage;
  }

  @override
  double skill2() {
    totalDamage = (getAttack * (30 / 2)) / 10;
    print("$name ฉุนเฉียวใส่เปิด live ด่า สร้างความเสียหาย 30");
    return totalDamage;
  }

  @override
  double skill3() {
    totalDamage = (getAttack * (50 / 2)) / 10;
    print("$name เรียกพวกมาหนุนรุมด่า สร้างความเสียหาย 50");
    return totalDamage;
  }

  @override
  String toString() {
    return "$name" " Attack : $attack" " HP : $hp";
  }

  @override
  double normalAttack() {
    totalDamage = (getAttack * (1 / 2)) / 10;
    print("$name : โจมตีธรรมดา");
    return totalDamage;
  }
}

class Vit extends Warrior {
  String? name;
  double totalDamage = 0;
  Vit(String name) : super(30, 380) {
    this.name = name;
  }

  void regen() {
    print("$name regen 5 hp per turn");
    super.hp + 5;
  }

  @override
  double skill1() {
    totalDamage = (getAttack * (10 / 2)) / 10;
    print("$name นั่งหลับโชว์สร้างความเสียหายทางจิตใจให้แก่ศัตรู 10 damage");
    return totalDamage;
  }

  @override
  void skill2() {
    print(
        "$name ใช้ความไม่รู้ของเขาปัดป้องการโจมตีจากศัตรู ป้องกันความเสียหายที่ได้รับในเทิร์นนี้");
  }

  @override
  double skill3() {
    totalDamage = (getAttack * (40 / 2)) / 10;
    print("$name เรียกคนที่ใช้พยุงแขนออกไปรุมศัตรู สร้างความเสียหาย 40 damage");
    return totalDamage;
  }

  @override
  String toString() {
    return "$name" " Attack : $attack" " HP : $hp";
  }

  @override
  double normalAttack() {
    totalDamage = (getAttack * (1 / 2)) / 10;
    print("$name : โจมตีธรรมดา");
    return totalDamage;
  }
}

class Monster {
  String? name;
  int? atk;
  int? hp;
  double totalDamage = 0;

  Monster(String name, int atk, int hp) {
    this.name = name;
    this.atk = atk;
    this.hp = hp;
  }

  get getAtk => this.atk;

  get getHp => this.hp;

  set setHp(hp) => this.hp = hp;

  double normalAttack() {
    totalDamage = (getAtk * 1) / 2;
    print("$name : โจมตีธรรมดา");
    return totalDamage;
  }

  @override
  String toString() {
    return "$name" " HP : $hp";
  }
}

void main() {
  Monster boss = Monster("Boss no.1", 50, 500);
  Vit vit = Vit("Pra-vit");
  Pareena pareena = Pareena("Pareena");
  List<Object> listHero = heroList(vit, pareena);

  print("Choose your hero :");
  printListHero(listHero);

  int n = int.parse(stdin.readLineSync()!);
  print("Your hero is : ");
  print(listHero[n - 1]);

  print("-------------------");
  print("Stage 1");
  print("Monster: \n$boss");

  int c = 0;

  while (boss.hp! >= 0) {
    if (n == 1) {
      vit.menuList();
      int m = int.parse(stdin.readLineSync()!);
      c = vitAction1(m, vit, boss, c);
      c = vitAction2(m, vit, boss, c);
      c = vitAction3(m, vit, boss, c);
      c = vitAction4(m, vit, boss, c);
      vitAction5(m, vit, boss, c);
    }
    if (n == 2) {
      pareena.menuList();
      int m = int.parse(stdin.readLineSync()!);
      c = pareenaAction1(m, pareena, boss, c);
      c = pareenaAction2(m, pareena, boss, c);
      c = pareenaAction3(m, pareena, boss, c);
      c = pareenaAction4(m, pareena, boss, c);
      vitAction5(m, vit, boss, c);
    }
  }
}

void vitAction5(int m, Vit vit, Monster boss, int c) {
  if (m == 5) {
    vit.regen();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackVit(boss, vit);
    }
    c++;
  }
}

int vitAction4(int m, Vit vit, Monster boss, int c) {
  if (m == 4) {
    vit.skill3();
    boss.setHp = boss.hp! - vit.totalDamage.round();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackVit(boss, vit);
    }
    c++;
  }
  return c;
}

int vitAction3(int m, Vit vit, Monster boss, int c) {
  if (m == 3) {
    vit.skill2();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackVit(boss, vit);
    }
    c++;
  }
  return c;
}

int vitAction2(int m, Vit vit, Monster boss, int c) {
  if (m == 2) {
    vit.skill1();
    boss.setHp = boss.hp! - vit.totalDamage.round();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackVit(boss, vit);
    }
    c++;
  }
  return c;
}

int vitAction1(int m, Vit vit, Monster boss, int c) {
  if (m == 1) {
    vit.normalAttack();
    boss.setHp = boss.hp! - vit.totalDamage.round();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackVit(boss, vit);
    }
    c++;
  }
  return c;
}

void pareenaAction5(int m, Pareena pareena, Monster boss, int c) {
  if (m == 5) {
    pareena.perfectCast();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackPareena(boss, pareena);
    }
    c++;
  }
}

int pareenaAction4(int m, Pareena pareena, Monster boss, int c) {
  if (m == 4) {
    pareena.skill3();
    boss.setHp = boss.hp! - pareena.totalDamage.round();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackPareena(boss, pareena);
    }
    c++;
  }
  return c;
}

int pareenaAction3(int m, Pareena pareena, Monster boss, int c) {
  if (m == 3) {
    pareena.skill2();
    boss.setHp = boss.hp! - pareena.totalDamage.round();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackPareena(boss, pareena);
    }
    c++;
  }
  return c;
}

int pareenaAction2(int m, Pareena pareena, Monster boss, int c) {
  if (m == 2) {
    pareena.skill1();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackPareena(boss, pareena);
    }
    c++;
  }
  return c;
}

int pareenaAction1(int m, Pareena pareena, Monster boss, int c) {
  if (m == 1) {
    pareena.normalAttack();
    print("Monster: \n$boss");
    print("---------------------");
    if (bossTurn(c)) {
      bossAttackPareena(boss, pareena);
    }
    c++;
  }
  return c;
}

void bossAttackVit(Monster boss, Vit vit) {
  boss.normalAttack();
  vit.setHp = vit.hp - boss.totalDamage.round();
  print("Hero: \n$vit");
  print("---------------------");
}

void bossAttackPareena(Monster boss, Pareena pareena) {
  boss.normalAttack();
  pareena.setHp = pareena.hp - boss.totalDamage.round();
  print("Hero: \n$pareena");
  print("---------------------");
}

List<Object> heroList(Vit vit, Pareena pareena) {
  List<Object> listHero = [];
  listHero.add(vit);
  listHero.add(pareena);
  return listHero;
}

void printListHero(List<Object> listHero) {
  for (int i = 0; i < listHero.length; i++) {
    stdout.write(i + 1);
    stdout.write(". ");
    print(listHero[i]);
  }
}

bool bossTurn(int c) {
  if (c.isEven) {
    return false;
  }
  return true;
}
